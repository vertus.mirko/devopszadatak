import sys
import hashlib
import pika
import os


def on_message(channel, method_frame, properties, body):
    print(body.strip())
    message = str(body.strip())
    h = hashlib.new('md5')
    h.update(str.encode(message))
    print(h.hexdigest())
    f = open("/data/demofile2.txt", "a+")
    f.write("\n")
    f.write(h.hexdigest())
    f.close()
    try:
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    except TypeError as e:
        pass

parameters = pika.ConnectionParameters(host='rabbitmq')


connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.basic_consume('task_queue', on_message)
try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()

