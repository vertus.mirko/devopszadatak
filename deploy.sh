docker pull mirkovertus/service1:1.0
docker pull mirkovertus/service2:1.0

docker run -d --rm --name rabbitmq --hostname rabbitmq -p 5672:5672 -p 15672:15672 --network rabbitnet rabbitmq:3.9-management
docker run -d --rm -p 8080:8080 --network rabbitnet mirkovertus/service2:1.0
curl --data "https://www.google.com" http://localhost:8080
docker run -d --rm -v $(pwd):/data --network rabbitnet mirkovertus/service1:1.0
