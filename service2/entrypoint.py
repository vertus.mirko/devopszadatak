import requests
import sys
import pika


message = requests.get(sys.stdin.readline()).text
print(message)

connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
channel = connection.channel()
channel.queue_declare(queue='task_queue')
channel.basic_publish(
    exchange='',
    routing_key='task_queue',
    body=message
    )
print(" [x] Sent %r" % message)
connection.close()
